const anchor = require("@project-serum/anchor");
const { SystemProgram } = anchor.web3;

const main = async () => {
  console.log("🚀 Starting test...");

  const provider = anchor.AnchorProvider.env();
  anchor.setProvider(provider);

  const program = anchor.workspace.EracounterSolana;

  const keypair = anchor.web3.Keypair.generate();

  let tx = await program.rpc.create({
    accounts: {
      counter: keypair.publicKey,
      user: provider.wallet.publicKey,
      systemProgram: SystemProgram.programId,
    },
    signers: [keypair],
  });

  console.log("📝 Your transaction signature", tx);

  let account = await program.account.counter.fetch(keypair.publicKey);
  console.log("Count", account.count);

  await program.rpc.increment({
    accounts: {
      counter: keypair.publicKey,
    },
  });

  await program.rpc.decrement({
    accounts: {
      counter: keypair.publicKey,
      authority: provider.wallet.publicKey,
    },
  });

  account = await program.account.counter.fetch(keypair.publicKey);
  console.log("Count", account.count);

  await program.rpc.decrement({
    accounts: {
      counter: keypair.publicKey,
      authority: provider.wallet.publicKey,
    },
  });

  account = await program.account.counter.fetch(keypair.publicKey);
  console.log("Count", account.count);

  await program.rpc.increment({
    accounts: {
      counter: keypair.publicKey,
      authority: keypair.publicKey,
    },
  });

  account = await program.account.counter.fetch(keypair.publicKey);
  console.log("Count", account.count);
};

const runMain = async () => {
  try {
    await main();
    process.exit(0);
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
};

runMain();
