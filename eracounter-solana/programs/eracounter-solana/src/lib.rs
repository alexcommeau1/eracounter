use anchor_lang::prelude::*;

declare_id!("HrCDqSzDL7UrG3oz6Ujbs4QXQdNk5wZZQkzXbSEyxb9b");

#[program]
pub mod eracounter_solana {
    use super::*;

    pub fn create(ctx: Context<Create>) -> Result<()> {
        let counter = &mut ctx.accounts.counter;
        counter.count = 0;
        Ok(())
    }

    pub fn increment(ctx: Context<Increment>) -> Result<()> {
        let counter = &mut ctx.accounts.counter;
        counter.count += 1;
        Ok(())
    }

    pub fn decrement(ctx: Context<Decrement>) ->  anchor_lang::Result<()> {
        let counter = &mut ctx.accounts.counter;
        if  counter.count > 0 {
            counter.count -= 1;
        }else{
            return Err(error!(ErrorCode::Min));
        }
        Ok(())
    }
}

// Le context dans lequel j'initialise
#[derive(Accounts)]
pub struct Create<'info> {
    #[account(init, payer = user, space = 8 + 40)]
    pub counter: Account<'info, Counter>,
    #[account(mut)]
    pub user: Signer<'info>,
    pub system_program: Program<'info, System>,
}

#[derive(Accounts)]
pub struct Increment<'info> {
    #[account(mut)]
    pub counter: Account<'info, Counter>,
}

#[derive(Accounts)]
pub struct Decrement<'info> {
    #[account(mut)]
    pub counter: Account<'info, Counter>,
}

#[error_code]
pub enum ErrorCode {
    #[msg("Counter cannot be decrement under 0.")]
    Min,
}

#[account]
pub struct Counter {
    pub count: u64,
}
