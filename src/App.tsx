import React, { useEffect, useState } from 'react';
import { Connection, PublicKey, clusterApiUrl } from '@solana/web3.js';
import { Program, AnchorProvider, web3 } from '@project-serum/anchor';
import CircularProgress from '@mui/material/CircularProgress';
import Modal from '@mui/material/Modal'
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import { Buffer } from 'buffer';
import logo from './assets/solana-logo-freelogovectors.net_.svg';

import './App.css';
import kp from './keypair.json'
let idl = require('./idl.json')

window.Buffer = Buffer;
const { SystemProgram } = web3;

const arr = Object.values(kp._keypair.secretKey)
const secret = new Uint8Array(arr)
const baseAccount = web3.Keypair.fromSecretKey(secret)
const programID = new PublicKey(idl.metadata.address);
const network = clusterApiUrl('devnet');

/**
 * Types
 */
const opts: any = {
  preflightCommitment: "processed"
}
declare global {
  interface Window {
    solana: any;
  }
}

/**
 * Style pour la modal
 */
const style = {
  position: 'absolute' as 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  color: "white",
  width: 400,
  bgcolor: 'rgb(18, 18, 18);',
  border: '2px solid #000',
  boxShadow: 24,
  pt: 2,
  px: 4,
  pb: 3,
};

/**
 *  Fonction pour recupérer le provider d'Anchor
 * 
 * @returns void
 */
const getProvider: any = () => {
  const connection: any = new Connection(network, opts.preflightCommitment);
  const provider: any = new AnchorProvider(
    connection, window.solana, opts.preflightCommitment,
  );
  return provider;
}

function App() {

  /**
   * On garde le wallet
   */
  const [walletAddress, setWalletAddress] = useState<string | null>(null)

  /**
   * Le compteur
   */
  const [counter, setCounter] = useState<number | null>(null);

  /**
   * Loading quand on discute avec la blockchain
   */
  const [counterLoading, setCounterLoading] = useState<boolean | null>(false);

  /**
   * State pour l'ouverture ou non de la modal (pas d'extension phantom)
   */
  const [getWalletExtension, setGetWalletExtension] = useState<boolean>(false);


  useEffect(() => {
    const onLoad = async () => {
      await checkIfWalletIsConnected();

    };
    window.addEventListener('load', onLoad);
    return () => window.removeEventListener('load', onLoad);
  }, []);

  useEffect(() => {
    if (walletAddress) {
      console.log('Fetching counter...');
      getCounter();
    }
  }, [walletAddress]);

  /**
   * Lors de la première fois et/ou changement du baseAccount
   */
  const createCounter: any = async () => {
    try {
      const provider = getProvider();
      const program = new Program(idl, programID, provider);

      await program.rpc.create({
        accounts: {
          counter: baseAccount.publicKey,
          user: provider.wallet.publicKey,
          systemProgram: SystemProgram.programId,
        },
        signers: [baseAccount]
      });

      console.log("Created a new Counter w/ address:", baseAccount.publicKey.toString())
      getCounter();

    } catch (error) {
      console.log("Error in createCounter: ", error)
    }
  }

  /**
   * Permet la decrement/incrementation du compteur
   * @flag dec or inc pour increment et decrement
   */
  const changeCounter: any = async (flag: String) => {
    try {
      const provider = getProvider();
      const program = new Program(idl, programID, provider);
      const accountOpts: any = {
        accounts: {
          counter: baseAccount.publicKey,
        },
      }

      setCounterLoading(true)
      let account = await program.account.counter.fetch(baseAccount.publicKey);

      if (flag === 'dec')
        await program.rpc.decrement(accountOpts);
      else
        await program.rpc.increment(accountOpts);

      account = await program.account.counter.fetch(baseAccount.publicKey);
      setCounter(account.count.toNumber())
      setCounterLoading(false)

    } catch (error: any) {
      if (error.error)
        alert(error.error.errorMessage)
      setCounterLoading(false)
    }
  }

  /**
   * Permet de fetch le compteur
   */
  const getCounter: any = async () => {
    try {
      const provider = getProvider();
      const program = new Program(idl, programID, provider);
      const account = await program.account.counter.fetch(baseAccount.publicKey);

      console.log("Got the account", account.count)
      setCounter(account.count.toNumber())

    } catch (error) {
      console.log("Error in getCounter: ", error)

    }
  }

  const connectWallet = async () => {
    const { solana }: any = window;

    if (solana) {
      const response = await solana.connect();
      console.log('Connected with Public Key:', response.publicKey.toString());
      setWalletAddress(response.publicKey.toString());
    }
  };

  const renderNotConnectedContainer = () => (
    <Button variant="contained" style={{ backgroundColor: "#EB54BC" }} onClick={() => connectWallet()}>CONNECT YOUR WALLET</Button>
  );

  const renderNoCounterCreatedContainer = () => (
    <Button variant="contained" style={{ backgroundColor: "#80ebff", color: "black" }} onClick={() => createCounter()}>CREATE YOUR COUNTER</Button>
  )

  const checkIfWalletIsConnected: any = async () => {

    try {
      const { solana }: any = window;

      if (solana) {
        if (solana.isPhantom) {
          console.log('Phantom wallet found!');
          const response = await solana.connect({ onlyIfTrusted: true });
          console.log(
            'Connected with Public Key:',
            response.publicKey.toString()
          );

          setWalletAddress(response.publicKey.toString());
        }
      } else {
        setGetWalletExtension(true)
      }
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div className="App">
      <div className="header-container" >
        <div className="logo-header-content"><img alt="logo" src={logo} />ERACOUNTER</div>

        <div className="right-header-content">
          <a href="https://www.era2140.com" target="_blank" rel="noreferrer">Era2140</a>
          <a href="https://solana.com" target="_blank" rel="noreferrer">Solana</a>
          <a href="https://www.rust-lang.org/fr" target="_blank" rel="noreferrer">Rust</a>
          <a href="https://github.com/project-serum/anchor" target="_blank" rel="noreferrer">Anchor</a>
        </div>

      </div>
      <div className="container">
        <div className="left-block">
          <p className="left-text">Powerful <br />Counter dev <br />for <span style={{ color: "rgb(20, 241, 149)" }}>Era</span><br />skills test.</p>
          <p className="left-bottom-text">A cool counter developped on solana<br /> blockchain made with love by Alex Commeau</p>
        </div>
        <div className="right-block">
          <div className="right-content">
            <p className="right-content-title">Eracounter</p>
            <p className="right-content-subtitle">Really easy, click on ADD to increment the counter and REMOVE to decrement.<br /> This counter can't be under 0.</p>
            <div className="sub-text">
              {counterLoading && <CircularProgress />}
              {walletAddress && counter !== null && !counterLoading && <p> {counter}</p>}
            </div>

            {!walletAddress && renderNotConnectedContainer()}
            {counter === null && walletAddress && renderNoCounterCreatedContainer()}

            {counter !== null && walletAddress && <>
              <div>
                <Button variant="contained" style={{ backgroundColor: "#494949", marginRight: "20px" }} onClick={() => changeCounter('dec')}>REMOVE</Button>
                <Button variant="contained" style={{ backgroundColor: "#EB54BC" }} onClick={() => changeCounter("inc")}>ADD</Button>
              </div>
            </>}

          </div>
        </div>

      </div>
      <Modal
        open={getWalletExtension}
        aria-labelledby="parent-modal-title"
        aria-describedby="parent-modal-description"
        disableAutoFocus={true}
      >
        <Box sx={{ ...style, width: 400 }}>
          <h2 id="parent-modal-title">Oupss</h2>
          <p id="parent-modal-description">
            Solana object not found!
          </p>
          <a href="https://phantom.app/download" target="_blank" style={{ textDecoration: "none", marginRight: "20px" }} rel="noreferrer">
            <Button variant="contained">Get a Phantom wallet</Button>
          </a>

          <Button variant="contained" onClick={() => checkIfWalletIsConnected()}>Retry</Button>

        </Box>
      </Modal>
    </div >
  );
}

export default App;
